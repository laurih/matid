Installation
============

The latest stable release is available through pip: (use the -\\-user flag if
root access is not available)

.. code-block:: sh

    pip install matid

To install the bleeding-edge development version, clone the source code from
gitlab and install with pip from local file:

.. code-block:: sh

    git clone https://gitlab.com/laurih/matid.git
    cd matid
    pip install .
