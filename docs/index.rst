MatID
=====

MatID is a python package for identifying and analyzing atomistic systems based
on their structure. MatID is designed to help researchers in the automated
analysis and labeling of atomistic datasets.

Capabilities at a Glance
========================

With MatID you can:

  - :doc:`Automatically analyze structural features in a dataset <tutorials/overview>`
  - :doc:`Automatically classify atomic geometries into different structural classes <tutorials/classification>`
  - Automatically identify outlier atoms such as adsorbates in surfaces geometries (tutorial in development)
  - Determine the dimensionality of an atomistic object (tutorial in development)
  - Analyze symmetry properties of 3D structures (tutorial in development)
  - Find surface atoms of structures (tutorial in development)

Check the tutorials to see more information.

Go Deeper
=========

Documentation for the source code :doc:`can be found here <doc/modules>`. The full source code can
be explored at `gitlab <https://gitlab.com/laurih/matid/>`_.

.. toctree::
    :hidden:

    install
    tutorials/tutorials
    Documentation <doc/modules>
    about
